第二天，我如約和莉莉一起前往斯巴達冒険者公會本部，接取了任務。在歡聲笑語的討論中得到的結果就是這個。

任務・討伐蠍尾獅
報酬・三千萬克蘭
期限・紅炎の月３１日
委託人・帕爾迪亞・海拉姆領主代理・凱恩塔斯
在原海拉姆北堡築巢的蠍尾獅封鎖住了道路。如果在紅炎の月內道路一直無法使用的話，會給海拉姆地區的貿易帶來巨大打擊。希望能盡快討伐。

「──蠍尾獅嗎？作為黒乃桑的練習對象，算得上是個合適的對手」

我們在午前回到了宅邸，在吃午飯時，菲奧娜點頭同意。

「菲奧娜知道蠍尾獅嗎？」
「我曾經打過兩次。第一次是幼體，第二次是還沒完全成為成體的年輕個體，所以並不怎麼強⋯⋯但長成完全的成體的話，相當強。即使在等級４怪物中也位於上層」

在聖艾利西昂魔法学院貫徹孤高直至畢業的菲奧娜，有著許多寶貴的經驗。我也在機動實驗中與種種怪物戰鬥過，但還沒打過蠍尾獅。
因此，在接受任務的時候，也好好地調查了一番。

蠍尾獅是奇美拉種的怪物，它的外觀特徵是有著像人一樣的臉龐，獅子的軀體，蠍子的尾巴，雖然不能飛但有著翅膀。據說在其長成為成體後，就大到了能一口吞下人類的程度，從頭到軀幹的長度將超過１０米，算上尾巴包含的話，長度還要翻倍。是十分出色的大型怪物。

僅僅是這巨軀作為怪物來說就有著十足的危險性，而且它還擁有著獅子之身的強韌之力與敏捷性，速度自然也相當的快。蠍子之尾也理所當然地具有劇毒，被刺中的話就等於吃了一發毒屬性中級魔法猛毒。而且尾巴尖端的毒刺就如毛髮般長得密密麻麻地，可以如機關槍一樣亂射。這已經是種出色的攻擊魔法了。

最麻煩的就是，它能以人面之口進行咏唱，使用火、雷、土魔法。根據個體不同，有可能只能有一種屬性的下級魔法，但最壊的情況下，也可能是三種屬性的上級魔法全都能用。而且能從通過咏唱使用魔法這點也能看出，它有著相當高的智商。

擁有著強韌肉體與魔法之力和狡猾頭腦的蠍尾獅，的確是無可挑剔的強敵。
但是，比不上《混沌熊兔》。除非這次的蠍尾獅也被像怠惰吉爾一樣的怪物給寄生強化了，否則就不會是《元素支配者》的對手。

「但是，地點是帕爾迪亞嗎？稍微有點遠。選更近一點的地方不是比較好嗎？」

是的，這次的任務既不是在斯巴達也不是在阿瓦隆，而是從未去過的国家的帕爾迪亞。
我是在報紙上讀到混沌熊兔出沒，才看到這個国家的名字的。那傢伙在這裡吃掉了迦樓羅，開始製造分身。

「雖然在斯巴達国內也有著很多任務，但是借此機會，我想去以前沒去過的其他国家看看。」
「想旅遊⋯⋯但你似乎不是這麼想的吧？」
「也許，能抓住『亞里亞修道會』的踪影。」

這裡雖然是有著魔法的異世界，但在傳遞信息的這一層面上看，遠遠不及網絡發達的現代地球。光是收集国內的信息就夠嗆，更何況是別国的事情，對於普通市民來說，這早已是未知領域了。

因此，即使亞里亞修道會在遠離斯巴達的国家中堂堂正正的活動著，我們也無法知道這件事。

「如果什麼都沒有的話，那就算了。但現在我們連他們的手伸到了什麼程度都不清楚」

認真思考的話，他們還只是以阿瓦隆為中心活動，最近，也出現在了斯巴達，這種程度而已。所以，他們應該還沒有伸手進帕爾迪亞⋯⋯

「我們不清楚他們那除了傳教以外的目的。」
「所以如果他們在像帕爾迪亞這種偏遠的国家中活動的話，他們很可能在為了這個謎之目的而行動。」
「嘛，就算真的存在，那我們也得找的到。」

實際上，如果這些傢伙是為了這秘密目的而行動的話，他們也會做好周密的隱瞞工作吧。想要一下子就看穿這些的話⋯⋯能靠莉莉的心靈感應能力做到嗎？

「不僅僅是帕爾迪亞，我想趁現在把這一帶的国家都看一看。」
「是啊，到了明年，不知何時十字軍就又會從代達羅斯發起進攻了。」

在加拉哈德大敗的十字軍，不可能輕易就重整好態勢。想要率領人數眾多的大軍前來攻打的話，那麼應該是要緩上一年的。
在這個緩衝期間內，我覺得就算我們遠離斯巴達也沒有問題。

「那麼，這次的成員呢？」
「因為是等級４任務，所以也沒必要出動全員。」
「那麼，我和黒乃桑兩個人去⋯⋯這麼說的話。也太任性了吧」
「我想三個人一起去」

我和莉莉和菲奧娜。是『元素支配者』的最早的成員。

「不，沒這個必要。就讓黒乃桑和莉莉桑兩個人去吧」
「真的嗎，難道說，這是菲奧娜你的關心嗎？」
「就算期待我的關心，我也沒自信能做好。」

這樣的話，為何說我和莉莉兩個人去就行了？不，她們不是那種能互相信賴的關係吧。

「那你為何這麼做？」
「我不知為何覺得這次這樣做比較好。硬要說的話，下次就和我兩個人去吧」

第一個理由十分曖昧，第二個理由就明確多了。

「這樣啊，是這樣嗎。這樣真的好嗎？」
「我和莉莉桑，有著很多要在斯巴達做的事。」

菲奧娜要在魔女工房裡進行研究，她也在和莉莉實際的戰鬥中展示出了各種各樣的成果。莉莉的工作就不更用說了。她都想回去迪士尼樂園研究了吧。

額，難道說，最閑的人，就是我嗎⋯⋯

「還有，今晚也請跟我一起睡吧。」
「明天早上就要出發。額，那啥，手下留情啊」

───

然後，在紅炎の月６日，我和莉莉兩人從斯巴達出發了。

這次是菲奧娜和沙利叶看家。希望沙利叶能和塞巴斯和羅登這兩個同為人造人的新人成為好朋友。
那麼，要去目的地帕爾迪亞，就要從斯巴達南下再穿過兩個国家。從距離上來看，這應該是迄今為止最遠的一次旅途了吧。

「在這條路上奔馳，真是讓人懷念啊。」

我一邊和莉莉共乘著梅莉，一邊眺望在道路兩旁延伸開來的伊斯基亞丘陵，情不自禁的自言自語道。

最初在這條路奔馳的時候，梅麗還只是普通的馬。為了尋找貪婪戈爾而在伊斯基亞丘陵轉來轉去，卻沒能找到，在正要回去的時候，卻遇上了法倫的盜賊團，並把他們擊潰了。
如果在我們擊潰了盜賊團之後，繼續尋找貪婪戈爾的話，說不定就能在它被怠惰吉爾寄生之前，或者說，在它製作怪物軍團之前，殺掉它了。

之所以會這麼想，是因為這全部都已經是過去的事情了吧。
雖然有著梅麗變成夢魘的痛苦回憶，但我跨越了試練，救下了朋友，而且還得到了巨大的名聲和等級５冒険者之位，所以伊斯基亞丘陵對我來說算是個美好的回憶之地。

「好懷念啊！」
「好懷念啊～」

我們像老人一樣，沉醉在這還不到一年的回憶中，越過伊斯基亞丘陵，終於踏入鄰国法倫。

斯巴達和法倫的邊境檢查所，就像一個城堡般，把道路全部封鎖起來。不，實際上就是個城堡吧。雖然感覺這建築太過森嚴，但因為在這個世界上也有著巨大的怪物，所以這種地方自然會修建地相當堅固。

我出示公會卡後，一下子就放行了。看來等級５的冒険者的確十分少見，法倫的警備兵都稍微吃了一驚。

說起來，警備兵全都是黒暗精靈啊。
聽說法倫是黒暗精靈的国家，這個褐膚銀髮的士兵排成一排的光景，該說是異国風情嗎，滿是異世界的感覺。而且，看到他們，我自然的就想起了烏露絲拉。

那孩子過得還好嗎？失去蕾琪，我也走了，希望她不要太過悲傷。開拓村也成功重建好了嗎⋯⋯如果順利重建了的話，那麼十字軍的領地的確正在順利發展，從大局上來看，情況就有點糟了啊。

我抱著這樣複雜的想法，穿越了法倫。

如果只是比国土面積的話，這個国家要比斯巴達大，不過，他們東邊的国土大半都是深深的密林，只有極少一部分是国民的生活圈。從西側的首都向東，就逐漸變為鄉村，在大森林的深處，似乎有著到即使在現代也仍舉行著以活人為祭的儀式的德魯伊特一族的村子⋯⋯

總之，法倫就是如此的寬廣，而這次我們只是穿過這裡而已。如果有適合的任務委託的話，我的確想去他們的首都看看。
我們花了一天半橫穿法倫西部，抵達下一個国家。

───

都市国家托魯齊斯。

與斯巴達、法倫相比，這個国家正如字面意思一樣，是個領土面積與都市国家之名相對應的国家。雖然面積很小，但其卻是個緊鄰雷姆利亞海，海上貿易繁盛的港口城市。
雷姆利亞海指的是從西向東貫穿切開潘多拉大陸中部的海域。從地理形狀來看，就像是地中海。

雷姆利亞海最東端的海岸線位於斯巴達領地。之前舉辦騎士選拔的阿瓦隆的港口城市塞勒涅，位於雷姆利亞海東部的北側，我來到的托魯齊斯位於其對面的南側。在雷姆利亞海的中間，夾在塞勒涅和托魯齊斯中間的島嶼就是紅翼伯爵的盧恩。

以托魯齊斯為首，有著許多相似的港口城市国家沿著雷姆利亞海的海岸線建立。通過海上貿易發展，並發展成獨立的城市国家，他們有著這種相似的歷史原。

由於貿易關係，這一帶被稱為中部城市国家群，但是也有像阿瓦隆和斯巴達那樣，已經不能稱為都市国家的国家，所以到了現代，這已經不是很準確的稱呼了吧。但幸運的是，大国之間沒有爆發關鍵性的戰爭，所以至今仍保持著牢固的同盟關係，維持著和平。

從這方面來說，有著瞄準著斯巴達背後，潘多拉最發達的中部地區的代達羅斯這個野心勃勃的国家在虎視眈眈，也是維持這種關係的巨大原因之一吧。想要團結的話，最重要的果然是樹立共同的敵人嗎？

比起政治上的事，在海邊的托魯齊斯的海產看起來很好吃，這是目前最吸引我興趣的東西。在塞勒涅之時沒機會吃。
但畢竟不是來旅遊的，所以我們沒有仔細挑選店舖，只是隨便找了家店吃午飯。

───

「我開動了」
「我開動了！」

主菜是盛在大盤子中的生魚片。雖然店員是說只會上今早在港口卸下來的新鮮食物，但看來他似乎並沒有說謊。晶瑩剔透的白肉上，盛著脂肪的赤肉。盤子上盛著多種漂亮的生魚片，對清楚生魚之美味的日本人來說，這模樣已經讓人無比的期待了。

不過，雖然有著類似金槍魚啦，鰤魚啦，鰹魚之類的東西，但它們全都是名字和模樣都不同的異世界的魚。雖然做成生魚片的模樣很像，但是味道到底有多像可是另一個問題。伴隨著期待，我也有些不安。

在做出生魚片的魚成謎的同時，在附贈的佐料中，我也發現了幾個陌生的東西。
首先，芥末和蘿蔔泥這兩個我知道的東西⋯⋯但那個紫色的糊糊是什麼鬼啊？

而已，是有著作為基本調料的醬油，但也有著其他的紅的藍的，這種謎一樣的調料。我實在不想挑戰這個像史萊姆一樣的粘糊的藍色調料。

「⋯⋯哦，真好吃啊」
「嗯，莉莉喜歡這個～」

只要不冒険，老老實實地沾著醬油吃，就不會讓它們如字面那樣變得不好吃。

「好厲害，味道幾乎都和我猜想的一樣。除此之外的生魚片，也有著意料之外的味道」
「莉莉，放點這個」

和我熟悉的日本生魚片長相相似的存在，每種的味道都和日本的差不多。除此之外，那種只能認為是異世界特有的、有著我從未見過的模樣的生魚片，則有著至今為止我從未品嘗過的獨特風味。

但是，嗯，都很好吃。原來如此，每一種都非常適合做生魚片，有著獨特又美妙的滋味。看來我可以相信這家店的廚師的味覺啊。不用害怕會有奇怪的東西，只需盡情享受料理了。

哎呀，難得附上了佐料，那就用用吧。首先就用芥末吧。
我還是普通的能吃芥末。雖然姐姐就完全吃不了──在喚醒了我這小小回憶，將手伸向了佐料盤的那一瞬間。

「這個綠色的奶油看起來很好吃」

莉莉一邊說著，一邊用勺子舀著芥末，盛到裝在小盤子裡的生魚片上。在金槍魚模樣的赤肉上，放上了像播放的綜藝節目中的懲罰遊戲一樣的巨量芥末。
喂喂，騙人吧，不會吧。

「等一下，莉莉！那是⋯⋯」
「啊─」

但莉莉直接張大嘴巴將盛滿芥末的金槍魚放進口中。

「哇啊啊，莉莉，不要嚼，別吃，快吐出來！」
「嗯？」
「快點，吐出來，吐掉！」
「呸，呸」

我就像看到孩子錯把橡皮擦放進了口裡的母親那般拚命，總算讓她成功吐出來了。她的舌頭奇跡般的沒有碰到芥末，她的表情像是在說，她有點不太明白我為什麼要讓她把吃的東西吐出來不明白，但因為我這麼說，所以她就照做了。

「沒事嗎，莉莉，嘴裡不辣嗎？」
「不辣啊」
「這樣啊，但下次可不要放這麼多了。這傢伙叫芥末，是特別辣的香辛料」
「不辣，這個是芥茉」
「芥茉？」
「恩，芥茉」

什麼啊，這種像是山寨貨一樣的名字。

不，等一下，很難想像莉莉會撒謊。妖精不會撒謊。

難道這傢伙是和芥末一模一樣的另一種東西嗎⋯⋯

「啊，這是啥啊，一點都不辣。」

我試著稍微嘗了點，完全感覺不到芥末那個獨特的辣味。不僅如此，可以說幾乎沒有味道。如果硬要舉出比較的東西的話，那應該就是牛蒡吧。這個單獨吃並不好吃，而是用來升華其他東西的味道的。

也就是說，這個芥茉不是像芥末那樣只能少量添加的作料，而是像蘿蔔泥一樣要大量加入的佐料。

「芥末在這裡」

莉莉理所當然地，拿起最初送到座位上的佐料器皿，打開蓋子，裡面裝滿了和芥末一模一樣的淺綠色糊狀物，這的確是真正的芥末。即使不嘗，那衝鼻的味道才像我訴說著這就才是芥末。

「真的啊，我完全沒有注意到。」
「嘿嘿，莉莉會告訴你的！」

確實，要是以日本人的觀念先入為主的來吃的話，這頓飯可能有點危險。就在我想著要不要向得意的哼哼著的莉莉請教一下異世界的佐料之時，發生了我將我以為是蘿蔔泥的作料放了很多到生魚片上，但它其實是芥末級的香辛料，害我直接翻了白眼的事件。但總的來說，我們還是度過了美味又愉快的午餐時間。

───

我們吃著美食，順利的走過了托魯齊斯。既然是都市国家同盟中的国家，自然可以順利移動。

那麼在與莉莉度過了一周長的愉快的二人之旅後，紅炎の月１３日。我們總算抵達了作為目的地的帕爾迪亞。

帕爾迪亞的大半国土都是廣闊的草原地帶，是半人馬的国家。其首都巴比羅尼卡是林立著與阿瓦隆和斯巴達樣式完全不同的建築物的大城市，不過，但大多數国民都還過著活在這寬廣草原的游牧生活。給人的印象大概就是蒙古。

雖然對這與我至今為止見到的国家有著完全不同的文化的国家抱有興趣，但還是應該優先工作。此次的任務，要先和在首都巴比羅尼卡的委託人見面。這是作為委托人本人的要求，寫在了委託書上。他似乎是要詳細說明下情況。

雖然是第一次來巴比羅尼卡，但我一下就找到了冒険者公會。在窗口出示公會卡和委托書後，立刻就與委托人取得了聯繫。
我和莉莉一邊悠閑地吃著飯，一邊老實地等待委托人到來就行了。

「話雖如此⋯⋯這房間的構造」

公會領我們進去的房間，是帕爾迪亞冒険者公會本部的等級５冒険者專用的房間，構造十分出色。裝飾著畫有帕爾迪亞雄偉大草原的畫、裝飾著傳統工藝風格的金飾，非常華麗。

但是，這個国家的居民是半人馬，對，正是下半身是馬，上半身是人的亞人種。也就是說，建造的建築物都要配合他們所擁有的馬身，門、通道、樓梯都十分寬敞。以普通人的感覺來說，感覺相當不協調。

這和我在伊魯茲村看到的，符合哥布林尺寸的小房子時的感覺差不多。

因為阿瓦隆和斯巴達都是以普通人類標準建造的建築，所以我沒在意，但是在以其他亞人種為主的国家和地區，這種地方就會有很大不同。雖說這也是理所當然的事，但如果沒有親身體驗，就體會不到實感。

我一邊體會著這微妙的不適感受，一邊像成吉思汗一樣吃著羊肉和蔬菜炒在一起的午餐，等了大約兩個小時後。

「對不起，讓您久等了。我是委託人凱恩塔斯」

出現的是和我一樣有著黒髮的威風凜凜的青年。頭髮很長，扎成了辮子。這個髮型就是帕爾迪亞騎士的證明。就如武士的髮髻一樣。
除了這個髮型外，這人上半身還穿著緊繃的綠白雙色的制服，下半身那長著黒毛的馬身上則穿著鋼鐵足鎧。他那掛在腰間的長劍，一眼就能看出是製作精良的優秀裝備。

確實，在委託書上寫著「領主代理」，所以他也有著相應的身份吧。

「初次見面，我是等級５隊伍『元素支配者』的黒乃。」
「我是莉莉喲」
「能遇到像您這樣的英雄真是我的光榮。當聽說是您接受了我的任務時，我都懷疑是我聽錯了」

這並不是恭維話，而是真心的感激之語，凱恩塔斯的臉上亮著光芒。難道，我在帕爾迪亞也很有名？

「為什麼這麼對我？我這可是第一次來這個国家」
「據說您在塞勒涅討伐了混沌熊兔⋯⋯那個魔獸給我国帶來了巨大損害」
「原來如此，然後呢？」
「恩，其實我也加入了討伐隊伍。但十分丟臉的是，我這不成器之軀，什麼都做不了」

不，只是和那傢伙交手後還能平安地活著回來這點就夠強了吧。

「我也不是靠自己一人的力量打倒它的。是多虧了眾多同伴的協助」
「您竟然是個和外表相反的謙虛之人。真是不能輕信謠言啊」

喂，等一下，我在帕爾迪亞的傳言中是個什麼樣的人啊？還有，不要隨便就說什麼和外表相反啊。
雖然有很多讓人在意的地方，但沒有多問，而是繼續和委托人交談下去。

───

「──那麼，凱，關於工作」

經過一段時間的對話後，為了便於交談，我們就不再用敬語，我更是用上了暱稱凱來稱呼他，然後我隨意的問道。

「你作為領主代理提出了任務，是有著什麼隱情吧？」

這是我從一開始就十分在意的地方。

原本，如果怪物成為了堵塞街道的障礙的話，騎士團就應該立即行動。要是在斯巴達的話根本不會发任務，但帕爾迪亞的騎士團卻無動於衷，甚至都沒有當地的冒険者來挑戰這任務。

這個就能国家解決的事，自然會讓這個国家的騎士團和冒険者來做，根本不會去請別国的人幫忙。但儘管如此，卻出現了這種任務，那麼自然會有相應的情況，或者說它是當地無法解決的強大怪物？

「雖然很丟我国騎士團的臉面，但我們還是決定不去討伐蠍尾獅。」
「不會吧，這不是一国騎士團無法戰勝的對手吧，這只是等級４的怪物啊。」
「是的，並不是無法討伐它。但它是我們必須做好要付出相應犧牲的存在」
「但是，這就是騎士的工作吧？」
「因為時機不好啊。由於混沌熊兔的襲擊，精銳部隊受到了不少損失。雖然沒有全軍覆，但現在也有著因為要療傷而無法從神殿回來的人。在這種情況下，騎士團的主力就只能著重做其他更為緊急的任務了⋯⋯」
「結論就是，人手不足啊」
「真是不好意思」
「冒険者呢？」
「雖然有幾個隊伍去挑戰了，但完全派不上用。」

因此把接受任務的條件提高到等級４以上後，這下就連挑戰之人就沒有了。

「報酬是我能給出的最大金額了，但作為高等級的任務，這好像是別人連看都不會看一眼的金額。」

確實，以等級４以上的任務來說，以千萬為單位是理所當然的事，就算是以億為單位也不稀奇。
既然是數量稀少的高等級冒険者，自然十分容易的就能找到報酬更好的任務。報酬太少的話，做這種危險的任務是很不划算的。

「但是，就因為這些原因，就放任它不管嗎？這不是凱個人發佈這任務的問題吧」

我已經知道了凱的身份。

凱的本名是凱恩塔斯・海拉姆。也就是海拉姆領主的兒子。
據說他在接受著如何成為下一個領主的精英教育中，正在首都巴比羅尼卡的騎士團中學習。所以，現在凱的身份除了是領主之子外，也是一名騎士。

「關於這次的事，還是放任不管比較安全。」
「是這樣嗎？」
「蠍尾獅原本就是住在森林地帶中的怪物。在不久的將來，它就將自行離去」
「那它為什麼在哪裡築巢了呢？」
「雖然不知原因為何，但不幸的是，雌蠍尾獅在那裡產下了卵。」

它佔領舊城堡當做不錯的巢穴來使用，在孵化前絶對會死守在哪裡。
因此，等到卵全部孵化，它就會和幼體一起回到原先居住的森林吧。

「原來如此，比起強行打倒它，的確是默默等待它離開較好啊。」

雖然道路被封鎖住會造成經濟上的損失，但因為討伐而喪失的人命更是無法挽回的。既然對方是蠍尾獅，自然有著相應的危險度。隨意出手只是愚蠢之舉
蠍尾獅也不會離開作為巢穴的城堡，它都不會去侵擾附近的村子。

「因此，想要現在就趕走蠍尾獅，只不過是我私人的願望而已。」
「為什麼你不惜重金也想打到遲早會走的怪物？」
「從以前開始，我的故鄉海拉姆的財政就十分困難──」

凱說起的，他這領主一家的貧困回憶，真是聞者動容見者落淚。他為了能穿上現在這身不會讓人小看的騎士裝備，更是歷盡了千辛萬苦。
為了節約削減伙食費，他一直和領民一起過著只吃水和蓋璐巴的日子⋯⋯蓋璐巴是什麼啊，是什麼樣的食物啊，我完全想像不出。
為了解決這貧困的狀況，凱的父親經過種種努力，終於成功地開發出了可以賺錢的特產。

「──今年這個紅炎の月，正是起死回生的商機！」

現在正是把這個特產會出售到首都巴比羅尼卡的時期，如果過了紅炎の月⋯⋯似乎就會因為各種原因而破產了。

「情況我已經清楚了。我明白了」
「用這種任性又渺小的理由來拜託像你這樣有名的冒険者，真是讓人害臊⋯⋯但對我，對我父親，對我們海拉姆的領民來說，這又是比任何事都重要的，我們共同的夙願」
「對我來說，只要知道沒有不好的內幕就夠了。而且，這可是為故鄉著想的偉大理由吧」

真是件讓人不禁流淚的事啊。知道這些後還不努力的話，那還算得上是男人嗎。

「交給我吧，我一定會打倒蠍尾獅的。」
「啊啊，黒乃大人⋯⋯萬分感謝！」

我戰意高漲的與凱緊緊相握。

「⋯⋯呣～」

另一邊，莉莉睡得十分香甜。